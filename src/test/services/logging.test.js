'use strict'

let config = require('../../main/config/config')
let { log } = require('../../main/services/logging')
let fs = require('fs')
// const dateFormat = require('dateformat')

describe('Logging service', () => {
    let logObj = {}

    beforeEach(() => {
        logObj = {
            name: 'x1',
            age: 12
        }
    })

    test('debug calling', () => {
        expect(log.debug('Test debug str')).toBeCalled
        expect(log.debug(JSON.stringify(logObj))).toBeCalled
    })

    test('Check if log-file created', () => {                        
        console.log('config.env = ' + config.env)
        console.log('config.log.dir = ' + config.log.dir)        
        if (config.log.dir) {                        
            log.debug('trying log something')
            // let date = new Date()            
            // console.log(dateFormat(date, 'YYYY-MM-DD-HH'))            
            //let path = config.log.dir + '\\' + config.log.fileName
            // console.log(path)
            //expect(fs.existsSync(path)).toBe(true)
        }
    })
})