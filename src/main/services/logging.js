'use strict'

let config = require('../config/config')
const { createLogger, format, transports } = require('winston')
require('winston-daily-rotate-file')
const { combine, timestamp, label, prettyPrint, printf } = format

const myTimeStamp = timestamp({ format: 'YYYY-MM-DD HH:mm:ss' })
const myFormat = printf(({ level, message, label, timestamp }) => {
    return `${timestamp} ${level}: ${message}`;
})

let transportList = [new transports.Console()]
let transportDailyRotateFile

if (config.log.dir) {    
    transportDailyRotateFile = new transports.DailyRotateFile({
        filename: config.log.dir + '\\' + config.log.fileName,
        datePattern: 'YYYY-MM-DD-HH',
        maxSize: '10m',
        zippedArchive: true
        // maxFiles: '14d'  
    })
    transportList.push(transportDailyRotateFile)
}


// transportDailyRotateFile.on('rotate', function(oldFilename, newFilename) {
//   // do something fun
// })

const logger = createLogger({
    level: config.log.level, // info debug
    format: combine(
        //label({ label: '' }), //, //prettyPrint(),      
        myTimeStamp,
        myFormat
    ),
    //defaultMeta: { service: 'user-service' },
    transports: transportList
})

module.exports.log = logger