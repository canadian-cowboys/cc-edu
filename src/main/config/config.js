'use strict'

require('dotenv').config()

/**
 * Configuration object
 * ```js
 * config.env
 * ```
 * possible values: [development, test, production]
 * ```js
 * config.log.level 
 * ```
 * possible values = [info, debug, warn, error]
 */
const config = {
    env: process.env.NODE_ENV || 'dev',    
    log: {
        level: process.env.LOG_LEVEL || 'warn',
        dir: process.env.LOG_DIR,
        fileName: process.env.LOG_FILE_NAME || 'cc-edu-%DATE%.log',
    }
}

module.exports = config

