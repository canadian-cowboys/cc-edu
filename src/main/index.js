'use strict'

let config = require('./config/config')
let { log } = require('./services/logging')

log.info('start edu')
log.debug('hello world! Env = ' + config.env)
log.warn('test warning')
log.error('test error')
